import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { StompConfig, StompService } from '@stomp/ng2-stompjs';


const stompConfig: StompConfig = {
  // ?
  url: 'ws://127.0.0.1:15674/ws',

  headers: {
    login: 'guest',
    passcode: 'guest'
  },

  heartbeat_in: 0,
  heartbeat_out: 10000,
  reconnect_delay: 5000,
  debug: true
};

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    StompService,
    {
      provide: StompConfig,
      useValue: stompConfig
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
