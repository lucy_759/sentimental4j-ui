import { Component } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { StompService } from '@stomp/ng2-stompjs';
import { Message } from '@stomp/stompjs';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  hashtag: string;
  streaming = false;
  streamingUrl = 'http://localhost:8080/stream';
  stopUrl = 'http://localhost:8080/stop';
  subscription: Subscription;
  messages: Observable<Message>;
  tweets = [];

  constructor(private http: HttpClient, private stompService: StompService) { }

  isHashtagEmpty() {
    return !this.hashtag;
  }

  isStreaming() {
    return this.streaming;
  }

  startStreaming() {
    this.streaming = true;
    this.http.get(this.streamingUrl, {
      params: new HttpParams().set('hashtag', this.hashtag)
    }).forEach(a => console.log(a));
    this.subscribe();
  }

  stopStreaming() {
    this.hashtag = '';
    this.streaming = false;
    this.http.get(this.stopUrl).forEach(a => console.log(a));
    this.unsubscribe();
    this.tweets.forEach(t => console.log(t));
    this.tweets = [];
  }

  subscribe() {
    this.messages = this.stompService.subscribe('tweets');
    this.subscription = this.messages.subscribe((message: Message) => {
      this.tweets.push(message.body + '\n');
    });
    // stompSubscription.subscribe(message => console.log(message.body));
    console.log('start');
  }

  unsubscribe() {
    this.subscription.unsubscribe();
    this.subscription = null;
    this.messages = null;
  }
}
